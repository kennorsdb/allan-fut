'use strict';

/**
 * Module dependencies
 */
var equiposPolicy = require('../policies/equipo.server.policy'),
  equipos = require('../controllers/equipos.server.controller');

module.exports = function (app) {
  // Equipos collection routes
  app.route('/api/equipos').all(equiposPolicy.isAllowed)
    .get(equipos.list)
    .post(equipos.create);

  // Single equipo routes
  app.route('/api/equipos/:equipoId').all(equiposPolicy.isAllowed)
    .get(equipos.read)
    .put(equipos.update)
    .delete(equipos.delete);

  // Finish by binding the equipo middleware
  app.param('equipoId', equipos.equipoByID);
};
