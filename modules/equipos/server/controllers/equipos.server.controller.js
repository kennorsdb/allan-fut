'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Equipo = mongoose.model('Equipo'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create an equipo
 */
exports.create = function (req, res) {
  var equipo = new Equipo(req.body);
  equipo.user = req.user;

  equipo.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(equipo);
    }
  });
};

/**
 * Show the current equipo
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var equipo = req.equipo ? req.equipo.toJSON() : {};

  // Add a custom field to the Equipo, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Equipo model.
  equipo.isCurrentUserOwner = !!(req.user && equipo.user && equipo.user._id.toString() === req.user._id.toString());

  res.json(equipo);
};

/**
 * Update an equipo
 */
exports.update = function (req, res) {
  var equipo = req.equipo;

  equipo.title = req.body.title;
  equipo.content = req.body.content;

  equipo.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(equipo);
    }
  });
};

/**
 * Delete an equipo
 */
exports.delete = function (req, res) {
  var equipo = req.equipo;

  equipo.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(equipo);
    }
  });
};

/**
 * List of Equipos
 */
exports.list = function (req, res) {
  Equipo.find().sort('-created').populate('user', 'displayName').exec(function (err, equipos) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(equipos);
    }
  });
};

/**
 * Equipo middleware
 */
exports.equipoByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Equipo is invalid'
    });
  }

  Equipo.findById(id).populate('user', 'displayName').exec(function (err, equipo) {
    if (err) {
      return next(err);
    } else if (!equipo) {
      return res.status(404).send({
        message: 'No equipo with that identifier has been found'
      });
    }
    req.equipo = equipo;
    next();
  });
};
