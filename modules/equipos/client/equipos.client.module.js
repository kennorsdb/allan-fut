(function (app) {
  'use strict';

  app.registerModule('equipos', ['core']);// The core module is required for special route handling; see /core/client/config/core.client.routes
  app.registerModule('equipos.admin', ['core.admin']);
  app.registerModule('equipos.admin.routes', ['core.admin.routes']);
  app.registerModule('equipos.services');
  app.registerModule('equipos.routes', ['ui.router', 'core.routes', 'equipos.services']);
}(ApplicationConfiguration));
