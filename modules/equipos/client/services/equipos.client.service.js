(function () {
  'use strict';

  angular
    .module('equipos.services')
    .factory('EquiposService', EquiposService);

  EquiposService.$inject = ['$resource', '$log'];

  function EquiposService($resource, $log) {
    var Equipo = $resource('/api/equipos/:equipoId', {
      equipoId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });

    angular.extend(Equipo.prototype, {
      createOrUpdate: function () {
        var equipo = this;
        return createOrUpdate(equipo);
      }
    });

    return Equipo;

    function createOrUpdate(equipo) {
      if (equipo._id) {
        return equipo.$update(onSuccess, onError);
      } else {
        return equipo.$save(onSuccess, onError);
      }

      // Handle successful response
      function onSuccess(equipo) {
        // Any required internal processing from inside the service, goes here.
      }

      // Handle error response
      function onError(errorResponse) {
        var error = errorResponse.data;
        // Handle error internally
        handleError(error);
      }
    }

    function handleError(error) {
      // Log error
      $log.error(error);
    }
  }
}());
