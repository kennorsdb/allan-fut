(function () {
  'use strict';

  angular
    .module('equipos')
    .controller('EquiposListController', EquiposListController);

  EquiposListController.$inject = ['EquiposService'];

  function EquiposListController(EquiposService) {
    var vm = this;

    vm.equipos = EquiposService.query();
  }
}());
