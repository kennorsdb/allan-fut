(function () {
  'use strict';

  angular
    .module('equipos')
    .controller('EquiposController', EquiposController);

  EquiposController.$inject = ['$scope', 'equipoResolve', 'Authentication'];

  function EquiposController($scope, equipo, Authentication) {
    var vm = this;

    vm.equipo = equipo;
    vm.authentication = Authentication;

  }
}());
