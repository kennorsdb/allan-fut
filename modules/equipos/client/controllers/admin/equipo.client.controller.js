(function () {
  'use strict';

  angular
    .module('equipos.admin')
    .controller('EquiposAdminController', EquiposAdminController);

  EquiposAdminController.$inject = ['$scope', '$state', '$window', 'equipoResolve', 'CategoriasService', 'Authentication', 'Notification'];

  function EquiposAdminController($scope, $state, $window, equipo, CategoriasService, Authentication, Notification) {
    var vm = this;

    vm.equipo = equipo;
    vm.authentication = Authentication;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.categorias = CategoriasService.query();

    console.log(vm.categorias);

    // Remove existing Equipo
    function remove() {
      if ($window.confirm('¿Está seguro que lo desea eliminar?')) {
        vm.equipo.$remove(function () {
          $state.go('admin.equipos.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Equipo eliminado correctamente!' });
        });
      }
    }

    // Save Equipo
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.equipoForm');
        return false;
      }

      // Create a new equipo, or update the current instance
      vm.equipo.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('admin.equipos.list'); // should we send the User to the list or the updated Equipo's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Equipo saved successfully!' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Equipo save error!' });
      }
    }
  }
}());
