﻿(function () {
  'use strict';

  angular
    .module('equipos.admin')
    .controller('EquiposAdminListController', EquiposAdminListController);

  EquiposAdminListController.$inject = ['EquiposService'];

  function EquiposAdminListController(EquiposService) {
    var vm = this;

    vm.equipos = EquiposService.query();
  }
}());
