(function () {
  'use strict';

  angular
    .module('equipos.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('equipos', {
        abstract: true,
        url: '/equipos',
        template: '<ui-view/>'
      })
      .state('equipos.list', {
        url: '',
        templateUrl: '/modules/equipos/client/views/list-equipos.client.view.html',
        controller: 'EquiposListController',
        controllerAs: 'vm'
      })
      .state('equipos.view', {
        url: '/:equipoId',
        templateUrl: '/modules/equipos/client/views/view-equipo.client.view.html',
        controller: 'EquiposController',
        controllerAs: 'vm',
        resolve: {
          equipoResolve: getEquipo
        },
        data: {
          pageTitle: '{{ equipoResolve.title }}'
        }
      });
  }

  getEquipo.$inject = ['$stateParams', 'EquiposService'];

  function getEquipo($stateParams, EquiposService) {
    return EquiposService.get({
      equipoId: $stateParams.equipoId
    }).$promise;
  }
}());
