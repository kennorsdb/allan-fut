﻿(function () {
  'use strict';

  // Configuring the Equipos Admin module
  angular
    .module('equipos.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Gestionar Equipos',
      state: 'admin.equipos.list'
    });
  }
}());
