(function () {
  'use strict';

  angular
    .module('equipos')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    menuService.addMenuItem('topbar', {
      title: 'Equipos',
      state: 'equipos',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'equipos', {
      title: 'Lista de Equipos',
      state: 'equipos.list',
      roles: ['*']
    });
  }
}());
