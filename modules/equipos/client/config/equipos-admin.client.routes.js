﻿(function () {
  'use strict';

  angular
    .module('equipos.admin.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('admin.equipos', {
        abstract: true,
        url: '/equipos',
        template: '<ui-view/>'
      })
      .state('admin.equipos.list', {
        url: '',
        templateUrl: '/modules/equipos/client/views/admin/list-equipos.client.view.html',
        controller: 'EquiposAdminListController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.equipos.create', {
        url: '/create',
        templateUrl: '/modules/equipos/client/views/admin/form-equipo.client.view.html',
        controller: 'EquiposAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          equipoResolve: newEquipo
        }
      })
      .state('admin.equipos.edit', {
        url: '/:equipoId/edit',
        templateUrl: '/modules/equipos/client/views/admin/form-equipo.client.view.html',
        controller: 'EquiposAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin'],
          pageTitle: '{{ equipoResolve.title }}'
        },
        resolve: {
          equipoResolve: getEquipo
        }
      });
  }

  getEquipo.$inject = ['$stateParams', 'EquiposService'];

  function getEquipo($stateParams, EquiposService) {
    return EquiposService.get({
      equipoId: $stateParams.equipoId
    }).$promise;
  }

  newEquipo.$inject = ['EquiposService'];

  function newEquipo(EquiposService) {
    return new EquiposService();
  }
}());
