'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Equipoe Schema
 */
var EquipoeSchema = new Schema({
  nombre: {
    type: String,
    default: '',
    required: 'Complete el nombre del equipo',
    trim: true
  },
  apellidos: {
    type: String,
    default: '',
    required: 'Complete los apellidos del equipo',
    trim: true
  },
  categoria: {
    type: String,
    default: '',
    //required: 'Seleccione una categoría',
    trim: true
  },
  fechaNacimiento: {
    type: Date,
    default: '',
    //required: 'Complete la fecha de nacimiento',
    trim: true
  },
  nacionalidad: {
    type: String,
    default: 'Costa Rica',
    //required: 'Seleccione la nacionalidad del equipo',
    trim: true
  },
  telefono: {
    type: Number,
    default: '',
  //  required: 'Complete el número de Teléfono',
    trim: true
  },
  email: {
    type: String,
    default: '',
    //required: 'Escriba una correo electrónico válido',
    trim: true
  },
  posicionEquipo: {
    type: String,
    default: '',
    //required: 'Seleccione la posición en la que juega el equipo',
    trim: true
  },
  numeroCamiseta: {
    type: Number,
    default: '',
    //required: 'Escriba el número de camiseta del equipo',
    trim: true
  },
  perfil: {
    type: String,
    default: '',
    //required: 'Seleccione el perfil del equipo',
    trim: true
  },
  peso: {
    type: Number,
    default: '',
    //required: 'Escriba el peso del equipo en kilogramos',
    trim: true
  },
  altura: {
    type: Number,
    default: '',
    //required: 'Escriba la altura del equipo en centímetros',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Equipoe', EquipoeSchema);
