﻿'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Equipo = mongoose.model('Equipo'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  equipo;

/**
 * Equipo routes tests
 */
describe('Equipo Admin CRUD tests', function () {
  before(function (done) {
    // Get application
    app = express.init(mongoose.connection.db);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      usernameOrEmail: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      roles: ['user', 'admin'],
      username: credentials.usernameOrEmail,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new equipo
    user.save()
      .then(function () {
        equipo = {
          title: 'Equipo Title',
          content: 'Equipo Content'
        };

        done();
      })
      .catch(done);
  });

  it('should be able to save an equipo if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new equipo
        agent.post('/api/equipos')
          .send(equipo)
          .expect(200)
          .end(function (equipoSaveErr, equipoSaveRes) {
            // Handle equipo save error
            if (equipoSaveErr) {
              return done(equipoSaveErr);
            }

            // Get a list of equipos
            agent.get('/api/equipos')
              .end(function (equiposGetErr, equiposGetRes) {
                // Handle equipo save error
                if (equiposGetErr) {
                  return done(equiposGetErr);
                }

                // Get equipos list
                var equipos = equiposGetRes.body;

                // Set assertions
                (equipos[0].user._id).should.equal(userId);
                (equipos[0].title).should.match('Equipo Title');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to update an equipo if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new equipo
        agent.post('/api/equipos')
          .send(equipo)
          .expect(200)
          .end(function (equipoSaveErr, equipoSaveRes) {
            // Handle equipo save error
            if (equipoSaveErr) {
              return done(equipoSaveErr);
            }

            // Update equipo title
            equipo.title = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing equipo
            agent.put('/api/equipos/' + equipoSaveRes.body._id)
              .send(equipo)
              .expect(200)
              .end(function (equipoUpdateErr, equipoUpdateRes) {
                // Handle equipo update error
                if (equipoUpdateErr) {
                  return done(equipoUpdateErr);
                }

                // Set assertions
                (equipoUpdateRes.body._id).should.equal(equipoSaveRes.body._id);
                (equipoUpdateRes.body.title).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an equipo if no title is provided', function (done) {
    // Invalidate title field
    equipo.title = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new equipo
        agent.post('/api/equipos')
          .send(equipo)
          .expect(422)
          .end(function (equipoSaveErr, equipoSaveRes) {
            // Set message assertion
            (equipoSaveRes.body.message).should.match('Title cannot be blank');

            // Handle equipo save error
            done(equipoSaveErr);
          });
      });
  });

  it('should be able to delete an equipo if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new equipo
        agent.post('/api/equipos')
          .send(equipo)
          .expect(200)
          .end(function (equipoSaveErr, equipoSaveRes) {
            // Handle equipo save error
            if (equipoSaveErr) {
              return done(equipoSaveErr);
            }

            // Delete an existing equipo
            agent.delete('/api/equipos/' + equipoSaveRes.body._id)
              .send(equipo)
              .expect(200)
              .end(function (equipoDeleteErr, equipoDeleteRes) {
                // Handle equipo error error
                if (equipoDeleteErr) {
                  return done(equipoDeleteErr);
                }

                // Set assertions
                (equipoDeleteRes.body._id).should.equal(equipoSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a single equipo if signed in and verify the custom "isCurrentUserOwner" field is set to "true"', function (done) {
    // Create new equipo model instance
    equipo.user = user;
    var equipoObj = new Equipo(equipo);

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new equipo
        agent.post('/api/equipos')
          .send(equipo)
          .expect(200)
          .end(function (equipoSaveErr, equipoSaveRes) {
            // Handle equipo save error
            if (equipoSaveErr) {
              return done(equipoSaveErr);
            }

            // Get the equipo
            agent.get('/api/equipos/' + equipoSaveRes.body._id)
              .expect(200)
              .end(function (equipoInfoErr, equipoInfoRes) {
                // Handle equipo error
                if (equipoInfoErr) {
                  return done(equipoInfoErr);
                }

                // Set assertions
                (equipoInfoRes.body._id).should.equal(equipoSaveRes.body._id);
                (equipoInfoRes.body.title).should.equal(equipo.title);

                // Assert that the "isCurrentUserOwner" field is set to true since the current User created it
                (equipoInfoRes.body.isCurrentUserOwner).should.equal(true);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  afterEach(function (done) {
    Equipo.remove().exec()
      .then(User.remove().exec())
      .then(done())
      .catch(done);
  });
});
