﻿(function () {
  'use strict';

  describe('Equipos Route Tests', function () {
    // Initialize global variables
    var $scope,
      EquiposService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _EquiposService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      EquiposService = _EquiposService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('admin.equipos');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/equipos');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('List Route', function () {
        var liststate;
        beforeEach(inject(function ($state) {
          liststate = $state.get('admin.equipos.list');
        }));

        it('Should have the correct URL', function () {
          expect(liststate.url).toEqual('');
        });

        it('Should be not abstract', function () {
          expect(liststate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(liststate.templateUrl).toBe('/modules/equipos/client/views/admin/list-equipos.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          EquiposAdminController,
          mockEquipo;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('admin.equipos.create');
          $templateCache.put('/modules/equipos/client/views/admin/form-equipo.client.view.html', '');

          // Create mock equipo
          mockEquipo = new EquiposService();

          // Initialize Controller
          EquiposAdminController = $controller('EquiposAdminController as vm', {
            $scope: $scope,
            equipoResolve: mockEquipo
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.equipoResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/admin/equipos/create');
        }));

        it('should attach an equipo to the controller scope', function () {
          expect($scope.vm.equipo._id).toBe(mockEquipo._id);
          expect($scope.vm.equipo._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('/modules/equipos/client/views/admin/form-equipo.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          EquiposAdminController,
          mockEquipo;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('admin.equipos.edit');
          $templateCache.put('/modules/equipos/client/views/admin/form-equipo.client.view.html', '');

          // Create mock equipo
          mockEquipo = new EquiposService({
            _id: '525a8422f6d0f87f0e407a33',
            title: 'An Equipo about MEAN',
            content: 'MEAN rocks!'
          });

          // Initialize Controller
          EquiposAdminController = $controller('EquiposAdminController as vm', {
            $scope: $scope,
            equipoResolve: mockEquipo
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:equipoId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.equipoResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            equipoId: 1
          })).toEqual('/admin/equipos/1/edit');
        }));

        it('should attach an equipo to the controller scope', function () {
          expect($scope.vm.equipo._id).toBe(mockEquipo._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('/modules/equipos/client/views/admin/form-equipo.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());
