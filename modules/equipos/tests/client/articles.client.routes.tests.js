(function () {
  'use strict';

  describe('Equipos Route Tests', function () {
    // Initialize global variables
    var $scope,
      EquiposService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _EquiposService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      EquiposService = _EquiposService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('equipos');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/equipos');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('List Route', function () {
        var liststate;
        beforeEach(inject(function ($state) {
          liststate = $state.get('equipos.list');
        }));

        it('Should have the correct URL', function () {
          expect(liststate.url).toEqual('');
        });

        it('Should not be abstract', function () {
          expect(liststate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(liststate.templateUrl).toBe('/modules/equipos/client/views/list-equipos.client.view.html');
        });
      });

      describe('View Route', function () {
        var viewstate,
          EquiposController,
          mockEquipo;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('equipos.view');
          $templateCache.put('/modules/equipos/client/views/view-equipo.client.view.html', '');

          // create mock equipo
          mockEquipo = new EquiposService({
            _id: '525a8422f6d0f87f0e407a33',
            title: 'An Equipo about MEAN',
            content: 'MEAN rocks!'
          });

          // Initialize Controller
          EquiposController = $controller('EquiposController as vm', {
            $scope: $scope,
            equipoResolve: mockEquipo
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:equipoId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.equipoResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            equipoId: 1
          })).toEqual('/equipos/1');
        }));

        it('should attach an equipo to the controller scope', function () {
          expect($scope.vm.equipo._id).toBe(mockEquipo._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('/modules/equipos/client/views/view-equipo.client.view.html');
        });
      });

      describe('Handle Trailing Slash', function () {
        beforeEach(inject(function ($state, $rootScope, $templateCache) {
          $templateCache.put('/modules/equipos/client/views/list-equipos.client.view.html', '');

          $state.go('equipos.list');
          $rootScope.$digest();
        }));

        it('Should remove trailing slash', inject(function ($state, $location, $rootScope) {
          $location.path('equipos/');
          $rootScope.$digest();

          expect($location.path()).toBe('/equipos');
          expect($state.current.templateUrl).toBe('/modules/equipos/client/views/list-equipos.client.view.html');
        }));
      });
    });
  });
}());
