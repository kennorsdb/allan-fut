﻿'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Categoria = mongoose.model('Categoria'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  categoria;

/**
 * Categoria routes tests
 */
describe('Categoria Admin CRUD tests', function () {
  before(function (done) {
    // Get application
    app = express.init(mongoose.connection.db);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      usernameOrEmail: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      roles: ['user', 'admin'],
      username: credentials.usernameOrEmail,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new categoria
    user.save()
      .then(function () {
        categoria = {
          title: 'Categoria Title',
          content: 'Categoria Content'
        };

        done();
      })
      .catch(done);
  });

  it('should be able to save an categoria if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new categoria
        agent.post('/api/categorias')
          .send(categoria)
          .expect(200)
          .end(function (categoriaSaveErr, categoriaSaveRes) {
            // Handle categoria save error
            if (categoriaSaveErr) {
              return done(categoriaSaveErr);
            }

            // Get a list of categorias
            agent.get('/api/categorias')
              .end(function (categoriasGetErr, categoriasGetRes) {
                // Handle categoria save error
                if (categoriasGetErr) {
                  return done(categoriasGetErr);
                }

                // Get categorias list
                var categorias = categoriasGetRes.body;

                // Set assertions
                (categorias[0].user._id).should.equal(userId);
                (categorias[0].title).should.match('Categoria Title');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to update an categoria if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new categoria
        agent.post('/api/categorias')
          .send(categoria)
          .expect(200)
          .end(function (categoriaSaveErr, categoriaSaveRes) {
            // Handle categoria save error
            if (categoriaSaveErr) {
              return done(categoriaSaveErr);
            }

            // Update categoria title
            categoria.title = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing categoria
            agent.put('/api/categorias/' + categoriaSaveRes.body._id)
              .send(categoria)
              .expect(200)
              .end(function (categoriaUpdateErr, categoriaUpdateRes) {
                // Handle categoria update error
                if (categoriaUpdateErr) {
                  return done(categoriaUpdateErr);
                }

                // Set assertions
                (categoriaUpdateRes.body._id).should.equal(categoriaSaveRes.body._id);
                (categoriaUpdateRes.body.title).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an categoria if no title is provided', function (done) {
    // Invalidate title field
    categoria.title = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new categoria
        agent.post('/api/categorias')
          .send(categoria)
          .expect(422)
          .end(function (categoriaSaveErr, categoriaSaveRes) {
            // Set message assertion
            (categoriaSaveRes.body.message).should.match('Title cannot be blank');

            // Handle categoria save error
            done(categoriaSaveErr);
          });
      });
  });

  it('should be able to delete an categoria if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new categoria
        agent.post('/api/categorias')
          .send(categoria)
          .expect(200)
          .end(function (categoriaSaveErr, categoriaSaveRes) {
            // Handle categoria save error
            if (categoriaSaveErr) {
              return done(categoriaSaveErr);
            }

            // Delete an existing categoria
            agent.delete('/api/categorias/' + categoriaSaveRes.body._id)
              .send(categoria)
              .expect(200)
              .end(function (categoriaDeleteErr, categoriaDeleteRes) {
                // Handle categoria error error
                if (categoriaDeleteErr) {
                  return done(categoriaDeleteErr);
                }

                // Set assertions
                (categoriaDeleteRes.body._id).should.equal(categoriaSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a single categoria if signed in and verify the custom "isCurrentUserOwner" field is set to "true"', function (done) {
    // Create new categoria model instance
    categoria.user = user;
    var categoriaObj = new Categoria(categoria);

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new categoria
        agent.post('/api/categorias')
          .send(categoria)
          .expect(200)
          .end(function (categoriaSaveErr, categoriaSaveRes) {
            // Handle categoria save error
            if (categoriaSaveErr) {
              return done(categoriaSaveErr);
            }

            // Get the categoria
            agent.get('/api/categorias/' + categoriaSaveRes.body._id)
              .expect(200)
              .end(function (categoriaInfoErr, categoriaInfoRes) {
                // Handle categoria error
                if (categoriaInfoErr) {
                  return done(categoriaInfoErr);
                }

                // Set assertions
                (categoriaInfoRes.body._id).should.equal(categoriaSaveRes.body._id);
                (categoriaInfoRes.body.title).should.equal(categoria.title);

                // Assert that the "isCurrentUserOwner" field is set to true since the current User created it
                (categoriaInfoRes.body.isCurrentUserOwner).should.equal(true);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  afterEach(function (done) {
    Categoria.remove().exec()
      .then(User.remove().exec())
      .then(done())
      .catch(done);
  });
});
