'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Categoriae Schema
 */
var CategoriaeSchema = new Schema({
  nombre: {
    type: String,
    default: '',
    required: 'Complete el nombre del categoria',
    trim: true
  },
  apellidos: {
    type: String,
    default: '',
    required: 'Complete los apellidos del categoria',
    trim: true
  },
  categoria: {
    type: String,
    default: '',
    //required: 'Seleccione una categoría',
    trim: true
  },
  fechaNacimiento: {
    type: Date,
    default: '',
    //required: 'Complete la fecha de nacimiento',
    trim: true
  },
  nacionalidad: {
    type: String,
    default: 'Costa Rica',
    //required: 'Seleccione la nacionalidad del categoria',
    trim: true
  },
  telefono: {
    type: Number,
    default: '',
  //  required: 'Complete el número de Teléfono',
    trim: true
  },
  email: {
    type: String,
    default: '',
    //required: 'Escriba una correo electrónico válido',
    trim: true
  },
  posicionCategoria: {
    type: String,
    default: '',
    //required: 'Seleccione la posición en la que juega el categoria',
    trim: true
  },
  numeroCamiseta: {
    type: Number,
    default: '',
    //required: 'Escriba el número de camiseta del categoria',
    trim: true
  },
  perfil: {
    type: String,
    default: '',
    //required: 'Seleccione el perfil del categoria',
    trim: true
  },
  peso: {
    type: Number,
    default: '',
    //required: 'Escriba el peso del categoria en kilogramos',
    trim: true
  },
  altura: {
    type: Number,
    default: '',
    //required: 'Escriba la altura del categoria en centímetros',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Categoriae', CategoriaeSchema);
