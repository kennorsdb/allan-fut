'use strict';

/**
 * Module dependencies
 */
var categoriasPolicy = require('../policies/categoria.server.policy'),
  categorias = require('../controllers/categorias.server.controller');

module.exports = function (app) {
  // Categorias collection routes
  app.route('/api/categorias').all(categoriasPolicy.isAllowed)
    .get(categorias.list)
    .post(categorias.create);

  app.route('/api/categorias/entrenadores').all(categoriasPolicy.isAllowed)
    .all(categorias.entrenadores);

  // Single categoria routes
  app.route('/api/categorias/:categoriaId').all(categoriasPolicy.isAllowed)
    .get(categorias.read)
    .put(categorias.update)
    .delete(categorias.delete);

  // Finish by binding the categoria middleware
  app.param('categoriaId', categorias.categoriaByID);
};
