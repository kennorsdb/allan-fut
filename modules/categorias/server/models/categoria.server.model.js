'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  path = require('path'),
  config = require(path.resolve('./config/config')),
  chalk = require('chalk');

/**
 * Categoria Schema
 */
var CategoriaSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  nombre: {
    type: String,
    default: '',
    trim: true,
    required: 'El nombre no debe estar en blanco '
  },
  descripcion: {
    type: String,
    default: '',
    trim: true
    // required: 'Title cannot be blank'
  },
  responsable: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  fechaNacimientoMax: {
    type: Date,
    default: '',
    trim: true
  },
  fechaNacimientoMin: {
    type: Date,
    default: '',
    trim: true
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  escuela: {
    type: String,
    default: ''
  }

});

CategoriaSchema.statics.seed = seed;

mongoose.model('Categoria', CategoriaSchema);

/**
* Seeds the User collection with document (Categoria)
* and provided options.
*/
function seed(doc, options) {
  var Categoria = mongoose.model('Categoria');

  return new Promise(function (resolve, reject) {

    skipDocument()
      .then(findAdminUser)
      .then(add)
      .then(function (response) {
        return resolve(response);
      })
      .catch(function (err) {
        return reject(err);
      });

    function findAdminUser(skip) {
      var User = mongoose.model('User');

      return new Promise(function (resolve, reject) {
        if (skip) {
          return resolve(true);
        }

        User
          .findOne({
            roles: { $in: ['admin'] }
          })
          .exec(function (err, admin) {
            if (err) {
              return reject(err);
            }

            doc.user = admin;

            return resolve();
          });
      });
    }

    function skipDocument() {
      return new Promise(function (resolve, reject) {
        Categoria
          .findOne({
            nombre: doc.nombre
          })
          .exec(function (err, existing) {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove Categoria (overwrite)

            existing.remove(function (err) {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise(function (resolve, reject) {
        if (skip) {
          return resolve({
            message: chalk.yellow('Database Seeding: Categoria\t' + doc.nombre + ' skipped')
          });
        }

        var categoria = new Categoria(doc);

        categoria.save(function (err) {
          if (err) {
            return reject(err);
          }

          return resolve({
            message: 'Database Seeding: Categoria\t' + categoria.nombre + ' added'
          });
        });
      });
    }
  });
}
