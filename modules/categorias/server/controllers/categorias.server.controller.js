'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Categoria = mongoose.model('Categoria'),
  User = mongoose.model('User'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create an categoria
 */
exports.create = function (req, res) {
  var categoria = new Categoria(req.body);
  categoria.user = req.user;

  categoria.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(categoria);
    }
  });
};

/**
 * Show the current categoria
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var categoria = req.categoria ? req.categoria.toJSON() : {};

  // Add a custom field to the Categoria, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Categoria model.
  categoria.isCurrentUserOwner = !!(req.user && categoria.user && categoria.user._id.toString() === req.user._id.toString());

  res.json(categoria);
};

/**
 * Update an categoria
 */
exports.update = function (req, res) {
  var categoria = req.categoria;

  categoria.title = req.body.title;
  categoria.content = req.body.content;

  categoria.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(categoria);
    }
  });
};

/**
 * Delete an categoria
 */
exports.delete = function (req, res) {
  var categoria = req.categoria;

  categoria.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(categoria);
    }
  });
};

/**
 * List of Categorias
 */
exports.list = function (req, res) {
  Categoria.find().sort('-created').populate('user', 'displayName').exec(function (err, categorias) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(categorias);
    }
  });
};

/**
 * Categoria middleware
 */
exports.categoriaByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Categoria is invalid'
    });
  }

  Categoria.findById(id).populate('user', 'displayName').populate('responsable', 'displayName').exec(function (err, categoria) {
    if (err) {
      return next(err);
    } else if (!categoria) {
      return res.status(404).send({
        message: 'No categoria with that identifier has been found'
      });
    }
    req.categoria = categoria;
    next();
  });
};


exports.entrenadores = function (req, res) {
  User.find({ }, { displayName: 1 }).sort('-created').exec(function (err, users) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(users);
    }
  });
};
