﻿(function () {
  'use strict';

  angular
    .module('categorias.admin.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('admin.categorias', {
        abstract: true,
        url: '/categorias',
        template: '<ui-view/>'
      })
      .state('admin.categorias.list', {
        url: '',
        templateUrl: '/modules/categorias/client/views/admin/list-categorias.client.view.html',
        controller: 'CategoriasAdminListController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.categorias.create', {
        url: '/create',
        templateUrl: '/modules/categorias/client/views/admin/form-categoria.client.view.html',
        controller: 'CategoriasAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          categoriaResolve: newCategoria
        }
      })
      .state('admin.categorias.edit', {
        url: '/:categoriaId/edit',
        templateUrl: '/modules/categorias/client/views/admin/form-categoria.client.view.html',
        controller: 'CategoriasAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin'],
          pageTitle: '{{ categoriaResolve.nombre }}'
        },
        resolve: {
          categoriaResolve: getCategoria
        }
      });
  }

  getCategoria.$inject = ['$stateParams', 'CategoriasService'];

  function getCategoria($stateParams, CategoriasService) {
    return CategoriasService.get({
      categoriaId: $stateParams.categoriaId
    }).$promise;
  }

  newCategoria.$inject = ['CategoriasService'];

  function newCategoria(CategoriasService) {
    return new CategoriasService();
  }
}());
