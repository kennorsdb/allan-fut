(function () {
  'use strict';

  angular
    .module('categorias.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('categorias', {
        abstract: true,
        url: '/categorias',
        template: '<ui-view/>'
      })
      .state('categorias.list', {
        url: '',
        templateUrl: '/modules/categorias/client/views/list-categorias.client.view.html',
        controller: 'CategoriasListController',
        controllerAs: 'vm'
      })
      .state('categorias.view', {
        url: '/:categoriaId',
        templateUrl: '/modules/categorias/client/views/view-categoria.client.view.html',
        controller: 'CategoriasController',
        controllerAs: 'vm',
        resolve: {
          categoriaResolve: getCategoria
        },
        data: {
          pageTitle: '{{ categoriaResolve.title }}'
        }
      });
  }

  getCategoria.$inject = ['$stateParams', 'CategoriasService'];

  function getCategoria($stateParams, CategoriasService) {
    return CategoriasService.get({
      categoriaId: $stateParams.categoriaId
    }).$promise;
  }
}());
