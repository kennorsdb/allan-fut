﻿(function () {
  'use strict';

  // Configuring the Categorias Admin module
  angular
    .module('categorias.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Gestionar Categorias',
      state: 'admin.categorias.list'
    });
  }
}());
