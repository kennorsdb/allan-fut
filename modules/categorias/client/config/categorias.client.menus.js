(function () {
  'use strict';

  angular
    .module('categorias')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    menuService.addMenuItem('topbar', {
      title: 'Categorias',
      state: 'categorias',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'categorias', {
      title: 'Lista de Categorias',
      state: 'categorias.list',
      roles: ['*']
    });
  }
}());
