(function () {
  'use strict';

  angular
    .module('categorias')
    .controller('CategoriasListController', CategoriasListController);

  CategoriasListController.$inject = ['CategoriasService'];

  function CategoriasListController(CategoriasService) {
    var vm = this;

    vm.categorias = CategoriasService.query();
  }
}());
