(function () {
  'use strict';

  angular
    .module('categorias')
    .controller('CategoriasController', CategoriasController);

  CategoriasController.$inject = ['$scope', 'categoriaResolve', 'AdminService', 'Authentication'];

  function CategoriasController($scope, categoria, AdminService, Authentication) {
    var vm = this;

    vm.categoria = categoria;
    vm.authentication = Authentication;
    vm.users = AdminService.query();
  }
}());
