﻿(function () {
  'use strict';

  angular
    .module('categorias.admin')
    .controller('CategoriasAdminListController', CategoriasAdminListController);

  CategoriasAdminListController.$inject = ['CategoriasService'];

  function CategoriasAdminListController(CategoriasService) {
    var vm = this;

    vm.categorias = CategoriasService.query();
  }
}());
