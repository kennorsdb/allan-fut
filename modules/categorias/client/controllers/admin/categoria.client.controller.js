(function () {
  'use strict';

  angular
    .module('categorias.admin')
    .controller('CategoriasAdminController', CategoriasAdminController);

  CategoriasAdminController.$inject = ['$scope', '$state', '$window', 'categoriaResolve', 'AdminService', 'Authentication', 'Notification'];

  function CategoriasAdminController($scope, $state, $window, categoria, AdminService, Authentication, Notification) {
    var vm = this;

    vm.categoria = categoria;
    categoria.fechaNacimientoMin = new Date(categoria.fechaNacimientoMin);
    vm.authentication = Authentication;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.users = AdminService.query();
    // Remove existing Categoria
    function remove() {
      if ($window.confirm('¿Está seguro que lo desea eliminar?')) {
        vm.categoria.$remove(function () {
          $state.go('admin.categorias.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Categoría eliminada correctamente!' });
        });
      }
    }

    // Save Categoria
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.categoriaForm');
        return false;
      }
      console.log(vm.categoria);
      // Create a new categoria, or update the current instance
      vm.categoria.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('admin.categorias.list'); // should we send the User to the list or the updated Categoria's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Categoria saved successfully!' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Categoria save error!' });
      }
    }
  }
}());
