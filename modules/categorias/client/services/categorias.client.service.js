(function () {
  'use strict';

  angular
    .module('categorias.services')
    .factory('CategoriasService', CategoriasService);

  CategoriasService.$inject = ['$resource', '$log'];

  function CategoriasService($resource, $log) {
    var Categoria = $resource('/api/categorias/:categoriaId', {
      categoriaId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });

    angular.extend(Categoria.prototype, {
      createOrUpdate: function () {
        var categoria = this;
        return createOrUpdate(categoria);
      }
    });

    angular.extend(Categoria.prototype, {
      entrenadores: function () {
        return $resource('/api/categorias/entrenadores', null, {
          update: {
            method: 'GET'
          }
        });
      }
    });

    return Categoria;

    function createOrUpdate(categoria) {
      console.log(categoria);
      if (categoria._id) {
        return categoria.$update(onSuccess, onError);
      } else {
        return categoria.$save(onSuccess, onError);
      }

      // Handle successful response
      function onSuccess(categoria) {
        // Any required internal processing from inside the service, goes here.
      }

      // Handle error response
      function onError(errorResponse) {
        var error = errorResponse.data;
        // Handle error internally
        handleError(error);
      }
    }

    function handleError(error) {
      // Log error
      $log.error(error);
    }
  }
}());
