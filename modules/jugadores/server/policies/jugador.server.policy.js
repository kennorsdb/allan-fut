'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke Jugadores Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/api/jugadores',
      permissions: '*'
    }, {
      resources: '/api/jugadores/:jugadorId',
      permissions: '*'
    }, {
      resources: '/api/jugadores/:jugadorId/addbill',
      permissions: '*'
    }, {
      resources: '/api/jugadores/pagos',
      permissions: '*'
    }

    ]
  }, {
    roles: ['user'],
    allows: [{
      resources: '/api/jugadores',
      permissions: ['get']
    }, {
      resources: '/api/jugadores/:jugadorId',
      permissions: ['get']
    }]
  }, {
    roles: ['guest'],
    allows: [{
      resources: '/api/jugadores',
      permissions: ['get']
    }, {
      resources: '/api/jugadores/:jugadorId',
      permissions: ['get']
    }]
  }]);
};

/**
 * Check If Jugadores Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? req.user.roles : ['guest'];

  // If an jugador is being processed and the current user created it then allow any manipulation
  if (req.jugador && req.user && req.jugador.user && req.jugador.user.id === req.user.id) {
    return next();
  }

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
