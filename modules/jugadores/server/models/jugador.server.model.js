'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  path = require('path'),
  config = require(path.resolve('./config/config')),
  chalk = require('chalk');

/**
 * Subesquemas de Pagos
 */
var SubPagosSchema = new Schema({
  fecha: {
    type: Date,
    default: Date.now
  },
  estado: {
    type: String,
    default: 'Pendiente'
  },
  montoCancelado: {
    type: Number,
    default: 0
  },
  mesCobro: {
    type: Date,
    default: new Date().setDate(1)
  }

});

/**
 * jugador Schema
 */
var JugadorSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  nombre: {
    type: String,
    default: '',
    required: 'Complete el nombre del jugador',
    trim: true
  },
  apellidos: {
    type: String,
    default: '',
    required: 'Complete los apellidos del jugador',
    trim: true
  },
  categoria: {
    type: String,
    default: '',
    // required: 'Seleccione una categoría',
    trim: true
  },
  fechaNacimiento: {
    type: Date,
    default: '',
    // required: 'Complete la fecha de nacimiento',
    trim: true
  },
  nacionalidad: {
    type: String,
    default: 'Costa Rica',
    // required: 'Seleccione la nacionalidad del jugador',
    trim: true
  },
  telefono: {
    type: Number,
    default: '',
  //  required: 'Complete el número de Teléfono',
    trim: true
  },
  email: {
    type: String,
    default: '',
    // required: 'Escriba una correo electrónico válido',
    trim: true
  },
  posicionJugador: {
    type: String,
    default: '',
    // required: 'Seleccione la posición en la que juega el jugador',
    trim: true
  },
  numeroCamiseta: {
    type: Number,
    default: '',
    // required: 'Escriba el número de camiseta del jugador',
    trim: true
  },
  perfil: {
    type: String,
    default: '',
    // required: 'Seleccione el perfil del jugador',
    trim: true
  },
  peso: {
    type: Number,
    default: '',
    // required: 'Escriba el peso del jugador en kilogramos',
    trim: true
  },
  altura: {
    type: Number,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },

  ID: {
    type: Number,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },

  dirección: {
    type: String,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },

  telefonoEmergencia: {
    type: Number,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },

  poliza: {
    type: Boolean,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },

  fechaInscripcion: {
    type: Number,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },

  nivelEstudios: {
    type: String,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },

  nombrePadre: {
    type: String,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },

  nombreMadre: {
    type: String,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },

  cantHermanos: {
    type: Number,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },

  telefonoPadre: {
    type: Number,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },
  telefonoMadre: {
    type: Number,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },

  ocupacionPadre: {
    type: String,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },

  ocupacionMadre: {
    type: String,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },
  cantPersonasEnCasa: {
    type: Number,
    default: '',
    // required: 'Escriba la altura del jugador en centímetros',
    trim: true
  },
  recibos: [SubPagosSchema],
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

JugadorSchema.statics.seed = seed;

mongoose.model('Jugador', JugadorSchema);

/**
* Seeds the User collection with document (jugador)
* and provided options.
*/
function seed(doc, options) {
  var Jugador = mongoose.model('jugador');

  return new Promise(function (resolve, reject) {

    skipDocument()
      .then(findAdminUser)
      .then(add)
      .then(function (response) {
        return resolve(response);
      })
      .catch(function (err) {
        return reject(err);
      });

    function findAdminUser(skip) {
      var User = mongoose.model('User');

      return new Promise(function (resolve, reject) {
        if (skip) {
          return resolve(true);
        }

        User
          .findOne({
            roles: { $in: ['admin'] }
          })
          .exec(function (err, admin) {
            if (err) {
              return reject(err);
            }

            doc.user = admin;

            return resolve();
          });
      });
    }

    function skipDocument() {
      return new Promise(function (resolve, reject) {
        Jugador
          .findOne({
            nombre: doc.nombre
          })
          .exec(function (err, existing) {
            if (err) {
              return reject(err);
            }

            if (!existing) {
              return resolve(false);
            }

            if (existing && !options.overwrite) {
              return resolve(true);
            }

            // Remove jugador (overwrite)

            existing.remove(function (err) {
              if (err) {
                return reject(err);
              }

              return resolve(false);
            });
          });
      });
    }

    function add(skip) {
      return new Promise(function (resolve, reject) {
        if (skip) {
          return resolve({
            message: chalk.yellow('Database Seeding: jugador\t' + doc.title + ' skipped')
          });
        }

        var jugador = new Jugador(doc);

        jugador.save(function (err) {
          if (err) {
            return reject(err);
          }

          return resolve({
            message: 'Database Seeding: jugador\t' + jugador.title + ' added'
          });
        });
      });
    }
  });
}
