var path = require('path'),
  nodemailer = require('nodemailer'),
  XlsxPopulate = require('xlsx-populate');

exports.sendMail = function (jugador) {
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'allan.rojas.alvarado@gmail.com',
      pass: 'wxhrjbiotrflkzci'
    }
  });

  let ulimoRecibo = jugador.recibos[jugador.recibos.length - 1];
  let fechaRecibo = new Date(ulimoRecibo.mesCobro);
  let fecha = new Date(ulimoRecibo.fecha);

  var htmlG = "<!doctype html> "
    + "<html>"
    + "<head>"
    + "<meta charset='utf-8'>"
    + "</head>"
    + "<body>"
    + "<table cellpadding='0' cellspacing='0' border='0' class='GenericInsertedTable' width='65%' style='border: 1px solid #C3D7EB; ' align='center'>"
    + "<tbody>"
    + "  <tr>"
    + "    <td align='center' colspan='5'><hr>"
    + "      <strong>" + jugador.perfil + "</strong><br>"
    + "      Plaza de Deportes<br>"
    + "      Guadalupe de Cartago<br>"
    + "      Tel: 83481978<br>"
    + "      </td>"
    + "  </tr>"
    + "  <tr>"
    + "    <td colspan='2'><strong>Fecha:</strong></td>"
    + "    <td colspan='3'>" + fecha.getDate() + "/" + (fecha.getMonth() + 1) + "/" + fecha.getFullYear() + "</td>"
    + "  </tr>"
    + "  <tr>"
    + "    <td colspan='5'>&nbsp;</td>"
    + "  </tr>"
    + "  <tr>"
    + "    <td><strong>A:</strong></td>"
    + "    <td colspan='4'>" + jugador.nombre + " " + jugador.apellidos + "</td>"
    + "  </tr>"
    + "  <tr>"
    + "    <td></td>"
    + "    <td colspan='2'>Pago de Mensualidad " + "(" + (fechaRecibo.getMonth() + 1) + "-" + fechaRecibo.getFullYear() + ")" + "</td>"
    + "    <td></td>"
    + "    <td>" + ulimoRecibo.montoCancelado + " colones</td>"
    + "  </tr>"
    + "  <tr>"
    + "    <td colspan='3'></td>"
    + "    <td><strong>Sub Total</strong></td>"
    + "    <td>" + ulimoRecibo.montoCancelado + " colones</td>"
    + "  </tr>"
    + "  <tr>"
    + "    <td colspan='3'><br>"
    + "      <br></td>"
    + "    <td><strong>TOTAL</strong></td>"
    + "    <td>" + ulimoRecibo.montoCancelado + " colones</td>"
    + "  </tr>"
    + "</tbody>"
    + "</table>"
    + "</body>"
    + "</html>";

  var mailOptions = {
    from: 'allan.rojas.alvarado@gmail.com',
    to: jugador.email,
    subject: 'Recibo de la Escuela de Fútbol - Fecha: ' + fechaRecibo.getMonth() + '/' + fechaRecibo.getFullYear(),
    html: htmlG
  };

  let resp = null;
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      resp = error;
    } else {
      console.log('Email sent: ' + info.response);
      return null;
    }
  });

  return resp;
};

exports.reporte = function (data) {
  let result = [['Fecha de Pago', 'Categoría', 'Nombre', 'Apellidos', 'Fecha de Nacimiento', 'Mes', 'Monto']];
  let mes = [null, 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
  for (let reg in data) {
    result.push([data[reg].fecha, data[reg].categoria, data[reg].nombre, data[reg].apellidos,  data[reg].fechaNacimiento, mes[data[reg].mesCobro], data[reg].montoCancelado]);
  }
  console.log(data[0]);

  return XlsxPopulate.fromBlankAsync().then(workbook => {
    workbook.sheet(0).cell('A1').value(result);

    workbook.sheet(0).column('A').width(15).style({ 'numberFormat': 'dd/mm/yyyy' });
    workbook.sheet(0).column('B').width(15);
    workbook.sheet(0).column('C').width(30);
    workbook.sheet(0).column('D').width(30);
    workbook.sheet(0).column('E').width(15).style("numberFormat", "dd/mm/yyyy");
    workbook.sheet(0).column('F').width(15);
    workbook.sheet(0).column('G').width(15);

    return workbook.outputAsync();
  });

};
