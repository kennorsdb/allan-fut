'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Jugador = mongoose.model('Jugador'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  Pagos = require('./pagos.controller');

/**
 * Create an jugador
 */
exports.create = function (req, res) {
  var jugador = new Jugador(req.body);
  jugador.user = req.user;

  jugador.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(jugador);
    }
  });
};

/**
 * Show the current jugador
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var jugador = req.jugador ? req.jugador.toJSON() : {};

  // Add a custom field to the Jugador, for determining if the current User is the 'owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Jugador model.
  jugador.isCurrentUserOwner = !!(req.user && jugador.user && jugador.user._id.toString() === req.user._id.toString());

  res.json(jugador);
};

/**
 * Update an jugador
 */

exports.update = function (req, res) {
  var jugador = req.jugador;
  jugador.set(req.body);

  jugador.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(jugador);
      if (req.query.charge === 'true') {
        if (Pagos.sendMail(jugador)) {
          return res.status(422).send({ message: errorHandler.getErrorMessage(err) });
        }
      }
    }
  });
};

/**
 * Delete an jugador
 */
exports.delete = function (req, res) {
  var jugador = req.jugador;

  jugador.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(jugador);
    }
  });
};

/**
 * List of Jugadores
 */
exports.list = function (req, res) {
  Jugador.find().sort('-created').populate('user', 'displayName').exec(function (err, jugadores) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(jugadores);
    }
  });
};

/**
 * Registra un pago para una fecha dada
 */
exports.addPay = function (req, res) {
  var pago = {};
  pago.fecha = req.body.fecha;
  pago.monto = req.body.monto;
  pago.estado = req.body.estado;
  pago.mesCobro = req.body.mesCobro;

  var jugador = req.jugador;

  jugador.pagos.push(pago);
};


/**
 * Jugador middleware
 */
exports.jugadorByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Jugador is invalid'
    });
  }

  Jugador.findById(id).populate('user', 'displayName').populate('jugadores').exec(function (err, jugador) {
    if (err) {
      return next(err);
    } else if (!jugador) {
      return res.status(404).send({
        message: 'No jugador with that identifier has been found'
      });
    }
    req.jugador = jugador;
    next();
  });
};

exports.reportePagos = function (req, res, next) {
  let query = [{ $project: { nombre: 1, apellidos: 1, recibos: 1, _id: 0, fechaNacimiento: 1, categoria: 1 } },
  { $unwind: { path: '$recibos' } },
  {
    $project: {
      nombre: 1, apellidos: 1,  fechaNacimiento: 1, categoria: 1, mesCobro: { $month: '$recibos.mesCobro' },
      montoCancelado: '$recibos.montoCancelado', fecha: '$recibos.fecha'
    }
  }];

  let cursor = Jugador.aggregate(query, function (err, result) {
    if (err) {
      next(err);
    } else {
      Pagos.reporte(result).then(data => {
        res.attachment("Registro de Pagos.xlsx");
        res.send(data);
      })
        .catch(next);
    }
  });

};

