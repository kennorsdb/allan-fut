'use strict';

/**
 * Module dependencies
 */
var jugadoresPolicy = require('../policies/jugador.server.policy'),
  jugadores = require('../controllers/jugadores.server.controller');

module.exports = function (app) {
  // Jugadores collection routes
  app.route('/api/jugadores').all(jugadoresPolicy.isAllowed)
    .get(jugadores.list)
    .post(jugadores.create);

  app.route('/api/jugadores/pagos').all(jugadoresPolicy.isAllowed)
    .get(jugadores.reportePagos);

  // Single jugador routes
  app.route('/api/jugadores/:jugadorId').all(jugadoresPolicy.isAllowed)
    .get(jugadores.read)
    .put(jugadores.update)
    .delete(jugadores.delete);

  // Single jugador routes
  app.route('/api/jugadores/:jugadorId ').all(jugadoresPolicy.isAllowed)
    .get(jugadores.read)
    .put(jugadores.update)
    .delete(jugadores.delete);


  // Finish by binding the jugador middleware
  app.param('jugadorId', jugadores.jugadorByID);
};
