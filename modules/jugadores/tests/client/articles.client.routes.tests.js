(function () {
  'use strict';

  describe('Jugadores Route Tests', function () {
    // Initialize global variables
    var $scope,
      JugadoresService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _JugadoresService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      JugadoresService = _JugadoresService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('jugadores');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/jugadores');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('List Route', function () {
        var liststate;
        beforeEach(inject(function ($state) {
          liststate = $state.get('jugadores.list');
        }));

        it('Should have the correct URL', function () {
          expect(liststate.url).toEqual('');
        });

        it('Should not be abstract', function () {
          expect(liststate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(liststate.templateUrl).toBe('/modules/jugadores/client/views/list-jugadores.client.view.html');
        });
      });

      describe('View Route', function () {
        var viewstate,
          JugadoresController,
          mockJugador;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          viewstate = $state.get('jugadores.view');
          $templateCache.put('/modules/jugadores/client/views/view-jugador.client.view.html', '');

          // create mock jugador
          mockJugador = new JugadoresService({
            _id: '525a8422f6d0f87f0e407a33',
            title: 'An Jugador about MEAN',
            content: 'MEAN rocks!'
          });

          // Initialize Controller
          JugadoresController = $controller('JugadoresController as vm', {
            $scope: $scope,
            jugadorResolve: mockJugador
          });
        }));

        it('Should have the correct URL', function () {
          expect(viewstate.url).toEqual('/:jugadorId');
        });

        it('Should have a resolve function', function () {
          expect(typeof viewstate.resolve).toEqual('object');
          expect(typeof viewstate.resolve.jugadorResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(viewstate, {
            jugadorId: 1
          })).toEqual('/jugadores/1');
        }));

        it('should attach an jugador to the controller scope', function () {
          expect($scope.vm.jugador._id).toBe(mockJugador._id);
        });

        it('Should not be abstract', function () {
          expect(viewstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(viewstate.templateUrl).toBe('/modules/jugadores/client/views/view-jugador.client.view.html');
        });
      });

      describe('Handle Trailing Slash', function () {
        beforeEach(inject(function ($state, $rootScope, $templateCache) {
          $templateCache.put('/modules/jugadores/client/views/list-jugadores.client.view.html', '');

          $state.go('jugadores.list');
          $rootScope.$digest();
        }));

        it('Should remove trailing slash', inject(function ($state, $location, $rootScope) {
          $location.path('jugadores/');
          $rootScope.$digest();

          expect($location.path()).toBe('/jugadores');
          expect($state.current.templateUrl).toBe('/modules/jugadores/client/views/list-jugadores.client.view.html');
        }));
      });
    });
  });
}());
