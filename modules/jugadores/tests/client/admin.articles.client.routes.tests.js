﻿(function () {
  'use strict';

  describe('Jugadores Route Tests', function () {
    // Initialize global variables
    var $scope,
      JugadoresService;

    // We can start by loading the main application module
    beforeEach(module(ApplicationConfiguration.applicationModuleName));

    // The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
    // This allows us to inject a service but then attach it to a variable
    // with the same name as the service.
    beforeEach(inject(function ($rootScope, _JugadoresService_) {
      // Set a new global scope
      $scope = $rootScope.$new();
      JugadoresService = _JugadoresService_;
    }));

    describe('Route Config', function () {
      describe('Main Route', function () {
        var mainstate;
        beforeEach(inject(function ($state) {
          mainstate = $state.get('admin.jugadores');
        }));

        it('Should have the correct URL', function () {
          expect(mainstate.url).toEqual('/jugadores');
        });

        it('Should be abstract', function () {
          expect(mainstate.abstract).toBe(true);
        });

        it('Should have template', function () {
          expect(mainstate.template).toBe('<ui-view/>');
        });
      });

      describe('List Route', function () {
        var liststate;
        beforeEach(inject(function ($state) {
          liststate = $state.get('admin.jugadores.list');
        }));

        it('Should have the correct URL', function () {
          expect(liststate.url).toEqual('');
        });

        it('Should be not abstract', function () {
          expect(liststate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(liststate.templateUrl).toBe('/modules/jugadores/client/views/admin/list-jugadores.client.view.html');
        });
      });

      describe('Create Route', function () {
        var createstate,
          JugadoresAdminController,
          mockJugador;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          createstate = $state.get('admin.jugadores.create');
          $templateCache.put('/modules/jugadores/client/views/admin/form-jugador.client.view.html', '');

          // Create mock jugador
          mockJugador = new JugadoresService();

          // Initialize Controller
          JugadoresAdminController = $controller('JugadoresAdminController as vm', {
            $scope: $scope,
            jugadorResolve: mockJugador
          });
        }));

        it('Should have the correct URL', function () {
          expect(createstate.url).toEqual('/create');
        });

        it('Should have a resolve function', function () {
          expect(typeof createstate.resolve).toEqual('object');
          expect(typeof createstate.resolve.jugadorResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(createstate)).toEqual('/admin/jugadores/create');
        }));

        it('should attach an jugador to the controller scope', function () {
          expect($scope.vm.jugador._id).toBe(mockJugador._id);
          expect($scope.vm.jugador._id).toBe(undefined);
        });

        it('Should not be abstract', function () {
          expect(createstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(createstate.templateUrl).toBe('/modules/jugadores/client/views/admin/form-jugador.client.view.html');
        });
      });

      describe('Edit Route', function () {
        var editstate,
          JugadoresAdminController,
          mockJugador;

        beforeEach(inject(function ($controller, $state, $templateCache) {
          editstate = $state.get('admin.jugadores.edit');
          $templateCache.put('/modules/jugadores/client/views/admin/form-jugador.client.view.html', '');

          // Create mock jugador
          mockJugador = new JugadoresService({
            _id: '525a8422f6d0f87f0e407a33',
            title: 'An Jugador about MEAN',
            content: 'MEAN rocks!'
          });

          // Initialize Controller
          JugadoresAdminController = $controller('JugadoresAdminController as vm', {
            $scope: $scope,
            jugadorResolve: mockJugador
          });
        }));

        it('Should have the correct URL', function () {
          expect(editstate.url).toEqual('/:jugadorId/edit');
        });

        it('Should have a resolve function', function () {
          expect(typeof editstate.resolve).toEqual('object');
          expect(typeof editstate.resolve.jugadorResolve).toEqual('function');
        });

        it('should respond to URL', inject(function ($state) {
          expect($state.href(editstate, {
            jugadorId: 1
          })).toEqual('/admin/jugadores/1/edit');
        }));

        it('should attach an jugador to the controller scope', function () {
          expect($scope.vm.jugador._id).toBe(mockJugador._id);
        });

        it('Should not be abstract', function () {
          expect(editstate.abstract).toBe(undefined);
        });

        it('Should have templateUrl', function () {
          expect(editstate.templateUrl).toBe('/modules/jugadores/client/views/admin/form-jugador.client.view.html');
        });

        xit('Should go to unauthorized route', function () {

        });
      });

    });
  });
}());
