﻿'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Jugador = mongoose.model('Jugador'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  jugador;

/**
 * Jugador routes tests
 */
describe('Jugador Admin CRUD tests', function () {
  before(function (done) {
    // Get application
    app = express.init(mongoose.connection.db);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      usernameOrEmail: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      roles: ['user', 'admin'],
      username: credentials.usernameOrEmail,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new jugador
    user.save()
      .then(function () {
        jugador = {
          title: 'Jugador Title',
          content: 'Jugador Content'
        };

        done();
      })
      .catch(done);
  });

  it('should be able to save an jugador if logged in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new jugador
        agent.post('/api/jugadores')
          .send(jugador)
          .expect(200)
          .end(function (jugadorSaveErr, jugadorSaveRes) {
            // Handle jugador save error
            if (jugadorSaveErr) {
              return done(jugadorSaveErr);
            }

            // Get a list of jugadores
            agent.get('/api/jugadores')
              .end(function (jugadoresGetErr, jugadoresGetRes) {
                // Handle jugador save error
                if (jugadoresGetErr) {
                  return done(jugadoresGetErr);
                }

                // Get jugadores list
                var jugadores = jugadoresGetRes.body;

                // Set assertions
                (jugadores[0].user._id).should.equal(userId);
                (jugadores[0].title).should.match('Jugador Title');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to update an jugador if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new jugador
        agent.post('/api/jugadores')
          .send(jugador)
          .expect(200)
          .end(function (jugadorSaveErr, jugadorSaveRes) {
            // Handle jugador save error
            if (jugadorSaveErr) {
              return done(jugadorSaveErr);
            }

            // Update jugador title
            jugador.title = 'WHY YOU GOTTA BE SO MEAN?';

            // Update an existing jugador
            agent.put('/api/jugadores/' + jugadorSaveRes.body._id)
              .send(jugador)
              .expect(200)
              .end(function (jugadorUpdateErr, jugadorUpdateRes) {
                // Handle jugador update error
                if (jugadorUpdateErr) {
                  return done(jugadorUpdateErr);
                }

                // Set assertions
                (jugadorUpdateRes.body._id).should.equal(jugadorSaveRes.body._id);
                (jugadorUpdateRes.body.title).should.match('WHY YOU GOTTA BE SO MEAN?');

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should not be able to save an jugador if no title is provided', function (done) {
    // Invalidate title field
    jugador.title = '';

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new jugador
        agent.post('/api/jugadores')
          .send(jugador)
          .expect(422)
          .end(function (jugadorSaveErr, jugadorSaveRes) {
            // Set message assertion
            (jugadorSaveRes.body.message).should.match('Title cannot be blank');

            // Handle jugador save error
            done(jugadorSaveErr);
          });
      });
  });

  it('should be able to delete an jugador if signed in', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new jugador
        agent.post('/api/jugadores')
          .send(jugador)
          .expect(200)
          .end(function (jugadorSaveErr, jugadorSaveRes) {
            // Handle jugador save error
            if (jugadorSaveErr) {
              return done(jugadorSaveErr);
            }

            // Delete an existing jugador
            agent.delete('/api/jugadores/' + jugadorSaveRes.body._id)
              .send(jugador)
              .expect(200)
              .end(function (jugadorDeleteErr, jugadorDeleteRes) {
                // Handle jugador error error
                if (jugadorDeleteErr) {
                  return done(jugadorDeleteErr);
                }

                // Set assertions
                (jugadorDeleteRes.body._id).should.equal(jugadorSaveRes.body._id);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  it('should be able to get a single jugador if signed in and verify the custom "isCurrentUserOwner" field is set to "true"', function (done) {
    // Create new jugador model instance
    jugador.user = user;
    var jugadorObj = new Jugador(jugador);

    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        // Get the userId
        var userId = user.id;

        // Save a new jugador
        agent.post('/api/jugadores')
          .send(jugador)
          .expect(200)
          .end(function (jugadorSaveErr, jugadorSaveRes) {
            // Handle jugador save error
            if (jugadorSaveErr) {
              return done(jugadorSaveErr);
            }

            // Get the jugador
            agent.get('/api/jugadores/' + jugadorSaveRes.body._id)
              .expect(200)
              .end(function (jugadorInfoErr, jugadorInfoRes) {
                // Handle jugador error
                if (jugadorInfoErr) {
                  return done(jugadorInfoErr);
                }

                // Set assertions
                (jugadorInfoRes.body._id).should.equal(jugadorSaveRes.body._id);
                (jugadorInfoRes.body.title).should.equal(jugador.title);

                // Assert that the "isCurrentUserOwner" field is set to true since the current User created it
                (jugadorInfoRes.body.isCurrentUserOwner).should.equal(true);

                // Call the assertion callback
                done();
              });
          });
      });
  });

  afterEach(function (done) {
    Jugador.remove().exec()
      .then(User.remove().exec())
      .then(done())
      .catch(done);
  });
});
