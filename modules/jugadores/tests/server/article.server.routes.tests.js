'use strict';

var should = require('should'),
  request = require('supertest'),
  path = require('path'),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  Jugador = mongoose.model('Jugador'),
  express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app,
  agent,
  credentials,
  user,
  jugador;

/**
 * Jugador routes tests
 */
describe('Jugador CRUD tests', function () {

  before(function (done) {
    // Get application
    app = express.init(mongoose.connection.db);
    agent = request.agent(app);

    done();
  });

  beforeEach(function (done) {
    // Create user credentials
    credentials = {
      usernameOrEmail: 'username',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create a new user
    user = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'test@test.com',
      username: credentials.usernameOrEmail,
      password: credentials.password,
      provider: 'local'
    });

    // Save a user to the test db and create new jugador
    user.save()
      .then(function () {
        jugador = {
          title: 'Jugador Title',
          content: 'Jugador Content'
        };

        done();
      })
      .catch(done);
  });

  it('should not be able to save an jugador if logged in without the "admin" role', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        agent.post('/api/jugadores')
          .send(jugador)
          .expect(403)
          .end(function (jugadorSaveErr, jugadorSaveRes) {
            // Call the assertion callback
            done(jugadorSaveErr);
          });

      });
  });

  it('should not be able to save an jugador if not logged in', function (done) {
    agent.post('/api/jugadores')
      .send(jugador)
      .expect(403)
      .end(function (jugadorSaveErr, jugadorSaveRes) {
        // Call the assertion callback
        done(jugadorSaveErr);
      });
  });

  it('should not be able to update an jugador if signed in without the "admin" role', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        agent.post('/api/jugadores')
          .send(jugador)
          .expect(403)
          .end(function (jugadorSaveErr, jugadorSaveRes) {
            // Call the assertion callback
            done(jugadorSaveErr);
          });
      });
  });

  it('should be able to get a list of jugadores if not signed in', function (done) {
    // Create new jugador model instance
    var jugadorObj = new Jugador(jugador);

    // Save the jugador
    jugadorObj.save(function () {
      // Request jugadores
      agent.get('/api/jugadores')
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Array).and.have.lengthOf(1);

          // Call the assertion callback
          done();
        });

    });
  });

  it('should be able to get a single jugador if not signed in', function (done) {
    // Create new jugador model instance
    var jugadorObj = new Jugador(jugador);

    // Save the jugador
    jugadorObj.save(function () {
      agent.get('/api/jugadores/' + jugadorObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('title', jugador.title);

          // Call the assertion callback
          done();
        });
    });
  });

  it('should return proper error for single jugador with an invalid Id, if not signed in', function (done) {
    // test is not a valid mongoose Id
    agent.get('/api/jugadores/test')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'Jugador is invalid');

        // Call the assertion callback
        done();
      });
  });

  it('should return proper error for single jugador which doesnt exist, if not signed in', function (done) {
    // This is a valid mongoose Id but a non-existent jugador
    agent.get('/api/jugadores/559e9cd815f80b4c256a8f41')
      .end(function (req, res) {
        // Set assertion
        res.body.should.be.instanceof(Object).and.have.property('message', 'No jugador with that identifier has been found');

        // Call the assertion callback
        done();
      });
  });

  it('should not be able to delete an jugador if signed in without the "admin" role', function (done) {
    agent.post('/api/auth/signin')
      .send(credentials)
      .expect(200)
      .end(function (signinErr, signinRes) {
        // Handle signin error
        if (signinErr) {
          return done(signinErr);
        }

        agent.post('/api/jugadores')
          .send(jugador)
          .expect(403)
          .end(function (jugadorSaveErr, jugadorSaveRes) {
            // Call the assertion callback
            done(jugadorSaveErr);
          });
      });
  });

  it('should not be able to delete an jugador if not signed in', function (done) {
    // Set jugador user
    jugador.user = user;

    // Create new jugador model instance
    var jugadorObj = new Jugador(jugador);

    // Save the jugador
    jugadorObj.save(function () {
      // Try deleting jugador
      agent.delete('/api/jugadores/' + jugadorObj._id)
        .expect(403)
        .end(function (jugadorDeleteErr, jugadorDeleteRes) {
          // Set message assertion
          (jugadorDeleteRes.body.message).should.match('User is not authorized');

          // Handle jugador error error
          done(jugadorDeleteErr);
        });

    });
  });

  it('should be able to get a single jugador that has an orphaned user reference', function (done) {
    // Create orphan user creds
    var _creds = {
      usernameOrEmail: 'orphan',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create orphan user
    var _orphan = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'orphan@test.com',
      username: _creds.usernameOrEmail,
      password: _creds.password,
      provider: 'local',
      roles: ['admin']
    });

    _orphan.save(function (err, orphan) {
      // Handle save error
      if (err) {
        return done(err);
      }

      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var orphanId = orphan._id;

          // Save a new jugador
          agent.post('/api/jugadores')
            .send(jugador)
            .expect(200)
            .end(function (jugadorSaveErr, jugadorSaveRes) {
              // Handle jugador save error
              if (jugadorSaveErr) {
                return done(jugadorSaveErr);
              }

              // Set assertions on new jugador
              (jugadorSaveRes.body.title).should.equal(jugador.title);
              should.exist(jugadorSaveRes.body.user);
              should.equal(jugadorSaveRes.body.user._id, orphanId);

              // force the jugador to have an orphaned user reference
              orphan.remove(function () {
                // now signin with valid user
                agent.post('/api/auth/signin')
                  .send(credentials)
                  .expect(200)
                  .end(function (err, res) {
                    // Handle signin error
                    if (err) {
                      return done(err);
                    }

                    // Get the jugador
                    agent.get('/api/jugadores/' + jugadorSaveRes.body._id)
                      .expect(200)
                      .end(function (jugadorInfoErr, jugadorInfoRes) {
                        // Handle jugador error
                        if (jugadorInfoErr) {
                          return done(jugadorInfoErr);
                        }

                        // Set assertions
                        (jugadorInfoRes.body._id).should.equal(jugadorSaveRes.body._id);
                        (jugadorInfoRes.body.title).should.equal(jugador.title);
                        should.equal(jugadorInfoRes.body.user, undefined);

                        // Call the assertion callback
                        done();
                      });
                  });
              });
            });
        });
    });
  });

  it('should be able to get a single jugador if not signed in and verify the custom "isCurrentUserOwner" field is set to "false"', function (done) {
    // Create new jugador model instance
    var jugadorObj = new Jugador(jugador);

    // Save the jugador
    jugadorObj.save(function (err) {
      if (err) {
        return done(err);
      }
      agent.get('/api/jugadores/' + jugadorObj._id)
        .end(function (req, res) {
          // Set assertion
          res.body.should.be.instanceof(Object).and.have.property('title', jugador.title);
          // Assert the custom field "isCurrentUserOwner" is set to false for the un-authenticated User
          res.body.should.be.instanceof(Object).and.have.property('isCurrentUserOwner', false);
          // Call the assertion callback
          done();
        });
    });
  });

  it('should be able to get single jugador, that a different user created, if logged in & verify the "isCurrentUserOwner" field is set to "false"', function (done) {
    // Create temporary user creds
    var _creds = {
      usernameOrEmail: 'jugadorowner',
      password: 'M3@n.jsI$Aw3$0m3'
    };

    // Create user that will create the Jugador
    var _jugadorOwner = new User({
      firstName: 'Full',
      lastName: 'Name',
      displayName: 'Full Name',
      email: 'temp@test.com',
      username: _creds.usernameOrEmail,
      password: _creds.password,
      provider: 'local',
      roles: ['admin', 'user']
    });

    _jugadorOwner.save(function (err, _user) {
      // Handle save error
      if (err) {
        return done(err);
      }

      // Sign in with the user that will create the Jugador
      agent.post('/api/auth/signin')
        .send(_creds)
        .expect(200)
        .end(function (signinErr, signinRes) {
          // Handle signin error
          if (signinErr) {
            return done(signinErr);
          }

          // Get the userId
          var userId = _user._id;

          // Save a new jugador
          agent.post('/api/jugadores')
            .send(jugador)
            .expect(200)
            .end(function (jugadorSaveErr, jugadorSaveRes) {
              // Handle jugador save error
              if (jugadorSaveErr) {
                return done(jugadorSaveErr);
              }

              // Set assertions on new jugador
              (jugadorSaveRes.body.title).should.equal(jugador.title);
              should.exist(jugadorSaveRes.body.user);
              should.equal(jugadorSaveRes.body.user._id, userId);

              // now signin with the test suite user
              agent.post('/api/auth/signin')
                .send(credentials)
                .expect(200)
                .end(function (err, res) {
                  // Handle signin error
                  if (err) {
                    return done(err);
                  }

                  // Get the jugador
                  agent.get('/api/jugadores/' + jugadorSaveRes.body._id)
                    .expect(200)
                    .end(function (jugadorInfoErr, jugadorInfoRes) {
                      // Handle jugador error
                      if (jugadorInfoErr) {
                        return done(jugadorInfoErr);
                      }

                      // Set assertions
                      (jugadorInfoRes.body._id).should.equal(jugadorSaveRes.body._id);
                      (jugadorInfoRes.body.title).should.equal(jugador.title);
                      // Assert that the custom field "isCurrentUserOwner" is set to false since the current User didn't create it
                      (jugadorInfoRes.body.isCurrentUserOwner).should.equal(false);

                      // Call the assertion callback
                      done();
                    });
                });
            });
        });
    });
  });

  afterEach(function (done) {
    Jugador.remove().exec()
      .then(User.remove().exec())
      .then(done())
      .catch(done);
  });
});
