(function (app) {
  'use strict';

  app.registerModule('jugadores', ['core']);// The core module is required for special route handling; see /core/client/config/core.client.routes
  app.registerModule('jugadores.admin', ['core.admin']);
  app.registerModule('jugadores.admin.routes', ['core.admin.routes']);
  app.registerModule('jugadores.services');
  app.registerModule('jugadores.routes', ['ui.router', 'core.routes', 'jugadores.services']);
}(ApplicationConfiguration));
