(function () {
  'use strict';

  angular
    .module('jugadores')
    .controller('JugadoresController', JugadoresController);

  JugadoresController.$inject = ['$scope', 'jugadorResolve', 'Authentication'];

  function JugadoresController($scope, jugador, Authentication) {
    var vm = this;

    vm.jugador = jugador;
    vm.authentication = Authentication;

  }
}());
