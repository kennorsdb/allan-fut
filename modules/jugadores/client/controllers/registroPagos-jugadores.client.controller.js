(function () {
  'use strict';

  angular
    .module('jugadores')
    .controller('JugadoresListPagosController', JugadoresListPagosController);

  JugadoresListPagosController.$inject = ['$window', '$http', 'JugadoresService', 'CategoriasService', 'Notification'];

  function JugadoresListPagosController($window, $http, JugadoresService, CategoriasService, Notification) {

    var vm = this;
    vm.mesCobro = new Date();
    vm.pagos = [];
    vm.jugadores = [];
    vm.montoTotal = 9000;
    vm.cat = null;
    vm.categorias = CategoriasService.query().$promise.then(res => { vm.cat = res[0].nombre; vm.categorias = res; });

    vm.descargarExcel = function () {
      window.open('/api/jugadores/pagos', '_blank');
    };


    vm.actualizarLista = function () {
      JugadoresService.query().$promise.then(function (jugadores) {
        vm.pagos = [];
        vm.jugadores = JSON.parse(angular.toJson(jugadores, false));
        for (var j in vm.jugadores) {
          if (vm.jugadores[j].recibos == null) {
            vm.pagos.push({ 'estado': 'Pendiente', 'jugador': vm.jugadores[j], 'parcial': false, 'monto': vm.montoTotal, 'porConfirmar': false });
          } else {
            var p = vm.jugadores[j].recibos.find(p => {
              p = new Date(p.mesCobro);
              return p.getMonth() === vm.mesCobro.getMonth()
                && p.getFullYear() === vm.mesCobro.getFullYear();
            });
            if (p != null) {
              vm.pagos.push({ 'estado': p.estado, 'jugador': vm.jugadores[j], 'parcial': false, 'monto': vm.montoTotal, 'porConfirmar': false });
            } else {
              vm.pagos.push({ 'estado': 'Pendiente', 'jugador': vm.jugadores[j], 'parcial': false, 'monto': vm.montoTotal, 'porConfirmar': false });
            }
          }
        }
      });
    };

    vm.actualizarLista();

    vm.pagar = function (pago) {
      var jugador = new JugadoresService(pago.jugador);
      var categoria = vm.categorias.find((d) => d.nombre === jugador.categoria);
      var f = new Date();

      if (!jugador.recibos) {
        jugador.recibos = [];
      }

      jugador.recibos.push({
        estado: pago.monto < vm.montoTotal ? 'Parcial' : 'Cancelado',
        montoCancelado: pago.monto,
        mesCobro: vm.mesCobro
      });


      jugador.perfil = categoria.escuela;

      if (jugador.email !== '') {
        jugador.$charge().then(
          () => Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Recibo enviado correctamente' }),
          () => Notification.error({ message: 'Problemas de envío del recibo por correo electrónico', title: '<i class="glyphicon glyphicon-remove"></i> Problemas para enviar recibo' })
        );
      } else {
        Notification.error({ message: 'Problemas de envío del recibo por correo electrónico', title: '<i class="glyphicon glyphicon-remove"></i> El jugador no cuenta con correo electrónico' });
      }

      pago.estado = pago.monto < vm.montoTotal ? 'Parcial' : 'Cancelado';
      if (pago.monto < vm.montoTotal) {
        pago.parcial = true;
      }
    };
  }


}());
