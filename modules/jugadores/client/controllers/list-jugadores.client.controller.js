(function () {
  'use strict';

  angular
    .module('jugadores')
    .controller('JugadoresListController', JugadoresListController);

  JugadoresListController.$inject = ['JugadoresService', 'CategoriasService'];

  function JugadoresListController(JugadoresService, CategoriasService) {
    var vm = this;
    vm.cat = null;
    vm.categorias = CategoriasService.query().$promise.then(res => {vm.cat = res[0].nombre; vm.categorias = res;});

    vm.jugadores = JugadoresService.query();
  }
}());
