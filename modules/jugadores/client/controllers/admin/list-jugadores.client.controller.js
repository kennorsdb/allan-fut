﻿(function () {
  'use strict';

  angular
    .module('jugadores.admin')
    .controller('JugadoresAdminListController', JugadoresAdminListController);

  JugadoresAdminListController.$inject = ['JugadoresService', 'CategoriasService'];

  function JugadoresAdminListController(JugadoresService, CategoriasService) {
    var vm = this;
    vm.cat = null;
    vm.categorias = CategoriasService.query().$promise.then(res => {vm.cat = res[0].nombre; vm.categorias = res;});

    vm.jugadores = JugadoresService.query();
  }
}());
