(function () {
  'use strict';

  angular
    .module('jugadores.admin')
    .controller('JugadoresAdminController', JugadoresAdminController);

  JugadoresAdminController.$inject = ['$scope', '$state', '$window', 'jugadorResolve', 'CategoriasService', 'Authentication', 'Notification'];

  function JugadoresAdminController($scope, $state, $window, jugador, categoriaService, Authentication, Notification) {
    var vm = this;

    vm.jugador = jugador;
    vm.authentication = Authentication;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.categorias = categoriaService.query();

    // Remove existing Jugador
    function remove() {
      if ($window.confirm('¿Está seguro que lo desea eliminar?')) {
        vm.jugador.$remove(function () {
          $state.go('admin.jugadores.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Jugador eliminado correctamente!' });
        });
      }
    }

    // Save Jugador
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.jugadorForm');
        return false;
      }

      // Create a new jugador, or update the current instance
      vm.jugador.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('admin.jugadores.list'); // should we send the User to the list or the updated Jugador's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> Jugador saved successfully!' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> Jugador save error!' });
      }
    }
  }
}());
