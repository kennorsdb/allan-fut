﻿(function () {
  'use strict';

  // Configuring the Jugadores Admin module
  angular
    .module('jugadores.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Gestionar Jugadores',
      state: 'admin.jugadores.list'
    });
  }
}());
