﻿(function () {
  'use strict';

  angular
    .module('jugadores.admin.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('admin.jugadores', {
        abstract: true,
        url: '/jugadores',
        template: '<ui-view/>'
      })
      .state('admin.jugadores.list', {
        url: '',
        templateUrl: '/modules/jugadores/client/views/admin/list-jugadores.client.view.html',
        controller: 'JugadoresAdminListController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.jugadores.create', {
        url: '/create',
        templateUrl: '/modules/jugadores/client/views/admin/form-jugador.client.view.html',
        controller: 'JugadoresAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          jugadorResolve: newJugador
        }
      })
      .state('admin.jugadores.edit', {
        url: '/:jugadorId/edit',
        templateUrl: '/modules/jugadores/client/views/admin/form-jugador.client.view.html',
        controller: 'JugadoresAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin'],
          pageTitle: '{{ jugadorResolve.nombre }}' + ' ' + '{{ jugadorResolve.apellidos }}'
        },
        resolve: {
          jugadorResolve: getJugador
        }
      });
  }

  getJugador.$inject = ['$stateParams', 'JugadoresService'];

  function getJugador($stateParams, JugadoresService) {
    return JugadoresService.get({
      jugadorId: $stateParams.jugadorId
    }).$promise;
  }

  newJugador.$inject = ['JugadoresService'];

  function newJugador(JugadoresService) {
    return new JugadoresService();
  }
}());
