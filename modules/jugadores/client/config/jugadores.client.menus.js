(function () {
  'use strict';

  angular
    .module('jugadores')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    menuService.addMenuItem('topbar', {
      title: 'Jugadores',
      state: 'jugadores',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'jugadores', {
      title: 'Lista de Jugadores',
      state: 'jugadores.list',
      roles: ['*']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'jugadores', {
      title: 'Pagos de Jugadores',
      state: 'jugadores.pagos',
      roles: ['admin']
    });
  }
}());
