(function () {
  'use strict';

  angular
    .module('jugadores.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('jugadores', {
        abstract: true,
        url: '/jugadores',
        template: '<ui-view/>'
      })
      .state('jugadores.list', {
        url: '',
        templateUrl: '/modules/jugadores/client/views/admin/list-jugadores.client.view.html',
        controller: 'JugadoresAdminListController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('jugadores.view', {
        url: '/:jugadorId',
        templateUrl: '/modules/jugadores/client/views/view-jugador.client.view.html',
        controller: 'JugadoresController',
        controllerAs: 'vm',
        resolve: {
          jugadorResolve: getJugador
        },
        data: {
          pageTitle: '{{ jugadorResolve.title }}'
        }
      })
      .state('jugadores.pagos', {
        url: '/jugadores/pagos',
        templateUrl: '/modules/jugadores/client/views/registroPagos-jugadores.client.view.html',
        controller: 'JugadoresListPagosController',
        controllerAs: 'vm'
      });
  }

  getJugador.$inject = ['$stateParams', 'JugadoresService'];

  function getJugador($stateParams, JugadoresService) {
    return JugadoresService.get({
      jugadorId: $stateParams.jugadorId
    }).$promise;
  }
}());
