(function () {
  'use strict';

  angular
    .module('jugadores.services')
    .factory('JugadoresService', JugadoresService);

  JugadoresService.$inject = ['$resource', '$log'];

  function JugadoresService($resource, $log) {
    var Jugador = $resource('/api/jugadores/:jugadorId', {
      jugadorId: '@_id'
    }, {
      update: {
        method: 'PUT'
      },
      charge: {
        method: 'PUT',
        params: { charge: true }
      }
    });

    angular.extend(Jugador.prototype, {
      createOrUpdate: function () {
        var jugador = this;
        return createOrUpdate(jugador);
      }
    });

    return Jugador;

    function createOrUpdate(jugador) {
      if (jugador._id) {
        return jugador.$update(onSuccess, onError);
      } else {
        return jugador.$save(onSuccess, onError);
      }

      // Handle successful response
      function onSuccess(jugador) {
        // Any required internal processing from inside the service, goes here.
      }

      // Handle error response
      function onError(errorResponse) {
        var error = errorResponse.data;
        // Handle error internally
        handleError(error);
      }
    }

    function handleError(error) {
      // Log error
      $log.error(error);
    }
  }
}());
