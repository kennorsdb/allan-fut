(function () {
  'use strict';

  angular
    .module('core')
    .controller('HeaderController', HeaderController);

  HeaderController.$inject = ['$scope', '$state', 'Authentication', 'menuService', '$timeout'];

  function HeaderController($scope, $state, Authentication, menuService, timeout) {
    var vm = this;

    vm.accountMenu = menuService.getMenu('account').items[0];
    vm.authentication = Authentication;
    vm.isCollapsed = false;
    vm.sidebarIsCollapsed = false;
    vm.menu = menuService.getMenu('topbar');

    $scope.$on('$stateChangeSuccess', stateChangeSuccess);

    function stateChangeSuccess() {
      // Collapsing the menu after navigation
      vm.isCollapsed = false;
      vm.sidebarIsCollapsed = false;
    }

    var sidebarTree = function () {
      $.AdminLTE.tree('.sidebar');
    };

    vm.hideSideBar = function hideSideBar() {
        // Enable sidebar push menu
      $('body').toggleClass('sidebar-collapse');
      $('body').toggleClass('sidebar-open');
    };

  }
}());
